# Group Cook

## handle group managed permissions
As part of short field trip you, Ray, Sheira, Zantana, Robert, J'onn, Bruce, Selina, Dinah, Oliver, Clark and Louis need a script hat can help you manage user and group permission so:
- those who wish to relax, need to have access, write and execute permissions to `relax` folder at the dining area (storage folder on external drive,usb or disk)
- those who wish to help out, can can have access, write and execute permissions to `prepare` area and to `relax` area as well.
- those who wish to setup the whole thing, can access to whole group in whole area as well as `prepare` and `relax` areas well.
- make sure that areas are available before hand, and that they are accessable from main system(`/home`)

###### NOTE:
- use fdisk and mkfs to setup the external disk/usb and there you should have `main` area and in it create `relax` and `prepare`
- setup all the users with your script and give them all encrypted password that is different from each other.
- setup groups according to the names set in description
- manage groups with set-group-id (folders need to have group permission)