# Shepard's Bash Pie

We'll preparing Shepard's bash pie tonight.
but to make it work we'll have to create a lot of small things and then combine them together:

- Heat it up the oven and check it every 3 mins if it reached 180 degrees celsius (cron job that runs a script that increases custom system variable OVEN_HEAT every 3 mins and stops when the value is equal to 180. the value should increase exponentially. you need to validate that cron is running.)
  
- Keep list of required ingredients for shepard's bash pie (create a script that requires you to enter values to array that is system variable. if the values are not correct, error should be provided ... yes you need to pre setup ingredients with script before hand)
  
- Setup storage for your ingredients (script that creates partition on external device called `fridge` where you keep your ingredients sorted alphabetically in a file called `shepard's_bash_pie.array`, after you have validated that they are in the array mentioned before-hand)

- Grind the ingredients and mix them up.(take every ingredients and create array of characters of each ingredients and then mix them, e.g   cheese, meat --> c h e e s e m e a t --> cemaetehse)

- cook it all. (set a cron job that uses script which purpose is to log the temperature of the oven and value of mixed ingredients list with time and date. )

- Once done, use tool named `figlet` to print the  `Shepard's Bash Pie` mixed ingredients and to wish the users name(Yes... you need to ask him/her for the name or pre-define the variable.) `bon appetite`.